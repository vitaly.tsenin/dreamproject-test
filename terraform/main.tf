terraform {
	required_providers {
		openstack = {
			source  = "terraform-provider-openstack/openstack"
      version = "~> 1.46.0"
		}
		vkcs = {
			source  = "vk-cs/vkcs"
      version = "0.1.6"
		}
	}
#  backend "s3" {
#    bucket = "terraform-state"
#    region = "us-east-1" # any valid AWS region
#    key    = "dev/my-project.tfstate"
#    endpoint = "https://hb.bizmrg.com/"
#    profile = "mcs" # profile in ~/.aws/credentials
#    skip_credentials_validation = true # https://github.com/hashicorp/terraform/issues/19733
#  }
}

provider "vkcs" {
    # Your user account.
    username = "vitalytsenin@yandex.ru"

    # The password of the account
    password = "9d2-8Gh-NVL-xFu"

    # The tenant token can be taken from the project Settings tab - > API keys.
    # Project ID will be our token.
    project_id = "376749c9f52948749a5bb148c33446ad"
    
    # Region name
    region = "RegionOne"
    
    auth_url = "https://infra.mail.ru:35357/v3/" 
}


provider "openstack" {
  # Your user account.
  user_name = "vitalytsenin@yandex.ru"

  # The password of the account
  password = "9d2-8Gh-NVL-xFu"

  # The tenant token can be taken from the project Settings tab - > API keys.
  # Project ID will be our token.
  tenant_id = "376749c9f52948749a5bb148c33446ad"

  # API endpoint
  # Terraform will use this address to access the MCS api.
  auth_url = "https://infra.mail.ru/identity/v3/"

  # use octavia to manage load balancers
  use_octavia = true
}

resource "openstack_networking_network_v2" "generic" {
  name = "dp-network"
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "local" {
  name            = "local"
  network_id      = openstack_networking_network_v2.generic.id
  cidr            = "10.0.0.0/24"
  dns_nameservers = ["8.8.8.8", "94.100.178.70"]
}

resource "openstack_networking_router_v2" "generic" {
  name                = "dp-router"
  external_network_id = "298117ae-3fa4-4109-9e08-8be5602be5a2"
  admin_state_up = "true"
}


# Router interface configuration
resource "openstack_networking_router_interface_v2" "local" {
  router_id = openstack_networking_router_v2.generic.id
  subnet_id = openstack_networking_subnet_v2.local.id
}

resource "openstack_networking_floatingip_v2" "dp-app1-ext-ip" {
  pool = "ext-net"
}

resource "openstack_compute_floatingip_associate_v2" "ip-app1-instance" {
  floating_ip = "${openstack_networking_floatingip_v2.dp-app1-ext-ip.address}"
  instance_id = "${openstack_compute_instance_v2.app1-instance.id}"
}

resource "openstack_blockstorage_volume_v2" "dp-app1-volume" {
  name        = "dp-app1-volume"
  volume_type = "ms1"
  size        = "20"
  image_id    = "e0144f62-cbac-4363-9c3d-dbc7ea799f6d"
}


resource "openstack_compute_instance_v2" "app1-instance" {
  name              = "app1"
  flavor_id         = "25ae869c-be29-4840-8e12-99e046d2dbd4"
  key_pair          = "${var.keypair_name}"
  availability_zone = "MS1"
  config_drive      = true

  security_groups = [
    "default",
    "ssh+www"
  ]
 
  block_device {
    uuid                  = "${openstack_blockstorage_volume_v2.dp-app1-volume.id}"
    source_type           = "volume"
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }

  metadata = {
    env = "APP"
  }

  network {
    uuid = "${openstack_networking_network_v2.generic.id}"
  }
}

resource "openstack_networking_floatingip_v2" "dp-app2-ext-ip" {
  pool = "ext-net"
}

resource "openstack_compute_floatingip_associate_v2" "ip-app2-instance" {
  floating_ip = "${openstack_networking_floatingip_v2.dp-app2-ext-ip.address}"
  instance_id = "${openstack_compute_instance_v2.app2-instance.id}"
}

resource "openstack_blockstorage_volume_v2" "dp-app2-volume" {
  name        = "dp-app2-volume"
  volume_type = "ms1"
  size        = "20"
  image_id    = "e0144f62-cbac-4363-9c3d-dbc7ea799f6d"
}


resource "openstack_compute_instance_v2" "app2-instance" {
  name              = "app2"
  flavor_id         = "25ae869c-be29-4840-8e12-99e046d2dbd4"
  key_pair          = "${var.keypair_name}"
  availability_zone = "MS1"
  config_drive      = true

  security_groups = [
    "default",
    "ssh+www"
  ]
 
  block_device {
    uuid                  = "${openstack_blockstorage_volume_v2.dp-app2-volume.id}"
    source_type           = "volume"
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }

  metadata = {
    env = "APP"
  }

  network {
    uuid = "${openstack_networking_network_v2.generic.id}"
  }

}

resource "openstack_networking_floatingip_v2" "dp-db-ext-ip" {
  pool = "ext-net"
}

resource "openstack_compute_floatingip_associate_v2" "ip-db-instance" {
  floating_ip = "${openstack_networking_floatingip_v2.dp-db-ext-ip.address}"
  instance_id = "${openstack_compute_instance_v2.db-instance.id}"
}

resource "openstack_blockstorage_volume_v2" "dp-db-volume" {
  name        = "dp-db-volume"
  volume_type = "ms1"
  size        = "20"
  image_id    = "e0144f62-cbac-4363-9c3d-dbc7ea799f6d"
}


resource "openstack_compute_instance_v2" "db-instance" {
  name              = "db"
  flavor_id         = "25ae869c-be29-4840-8e12-99e046d2dbd4"
  key_pair          = "${var.keypair_name}"
  availability_zone = "MS1"
  config_drive      = true

  security_groups = [
    "default",
    "ssh+www"
  ]
 
  block_device {
    uuid                  = "${openstack_blockstorage_volume_v2.dp-db-volume.id}"
    source_type           = "volume"
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }

  metadata = {
    env = "DB"
  }

  network {
    uuid = "${openstack_networking_network_v2.generic.id}"
  }

}

resource "openstack_networking_floatingip_v2" "dp-lb-ext-ip" {
  pool = "ext-net"
}

resource "openstack_compute_floatingip_associate_v2" "ip-lb-instance" {
  floating_ip = "${openstack_networking_floatingip_v2.dp-lb-ext-ip.address}"
  instance_id = "${openstack_compute_instance_v2.lb-instance.id}"
}

resource "openstack_blockstorage_volume_v2" "dp-lb-volume" {
  name        = "dp-lb-volume"
  volume_type = "ms1"
  size        = "20"
  image_id    = "e0144f62-cbac-4363-9c3d-dbc7ea799f6d"
}


resource "openstack_compute_instance_v2" "lb-instance" {
  name              = "lb"
  flavor_id         = "25ae869c-be29-4840-8e12-99e046d2dbd4"
  key_pair          = "${var.keypair_name}"
  availability_zone = "MS1"
  config_drive      = true

  security_groups = [
    "default",
    "ssh+www"
  ]
 
  block_device {
    uuid                  = "${openstack_blockstorage_volume_v2.dp-lb-volume.id}"
    source_type           = "volume"
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }

  metadata = {
    env = "LB"
  }

  network {
    uuid = "${openstack_networking_network_v2.generic.id}"
  }

}

resource "openstack_networking_floatingip_v2" "dp-mon-ext-ip" {
  pool = "ext-net"
}

resource "openstack_compute_floatingip_associate_v2" "ip-mon-instance" {
  floating_ip = "${openstack_networking_floatingip_v2.dp-mon-ext-ip.address}"
  instance_id = "${openstack_compute_instance_v2.mon-instance.id}"
}

resource "openstack_blockstorage_volume_v2" "dp-mon-volume" {
  name        = "dp-mon-volume"
  volume_type = "ms1"
  size        = "20"
  image_id    = "e0144f62-cbac-4363-9c3d-dbc7ea799f6d"
}


resource "openstack_compute_instance_v2" "mon-instance" {
  name              = "mon"
  flavor_id         = "25ae869c-be29-4840-8e12-99e046d2dbd4"
  key_pair          = "${var.keypair_name}"
  availability_zone = "MS1"
  config_drive      = true

  security_groups = [
    "default",
    "ssh+www"
  ]
 
  block_device {
    uuid                  = "${openstack_blockstorage_volume_v2.dp-mon-volume.id}"
    source_type           = "volume"
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }

  metadata = {
    env = "MON"
  }

  network {
    uuid = "${openstack_networking_network_v2.generic.id}"
  }

}


